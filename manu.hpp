#include <iostream>
#include "array_sequence.hpp"
#include "list_sequence.hpp"
#include "Sequence.hpp"
#include "funcs.hpp"
#include <stdexcept>

using namespace std;

enum Tipe {
    Int = 1,
    Float,
    Char,
    Back
};

enum Menu {
    menu = 1,
    Test_of_concat,
    Test_of_somthing,
    finish
};

void test_menu() {
    Tipe tp = Back;
    int tipe = tp;

    do {
        picktipemenu();
        cin>>tipe;
        switch(tipe) {
            case(Int):
                Intmenu();
                break;
            case(Float):
                Floatmenu();
                break;
            case(Char):
                Charmenu();
                break;
            case(Back):
                break;
            default:
                printf("Repid input");
                break;
        }
    } while (tipe != 4);
}

void Test_concat() {
    int a[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int b[] = {9, 8, 7, 6, 5, 4, 3};
    Sequence<int>* arr_1 = new ArraySequence<int>(a, 10);
    Sequence<int> * list_1 = new ListSequence<int>(b, 7);
    Sequence<int> * list_2 = new ListSequence<int>(a, 10);
    Sequence<int>* arr_2 = new ArraySequence<int>(b, 7);
    arr_1->Concat(arr_2);
    list_1->Concat(list_2);
    printf("List 1 + List 2(after concat) = ");
    printAll(list_1);
    printf("list 2(start list) = ");
    printAll(list_2);
    printf("Array 1 + Array 2 = ");
    printAll(arr_1);
    printf("Array 2(start array) = ");
    printAll(arr_2);
    delete arr_1;
    delete arr_2;
    delete list_1;
    delete list_2;
}

void Test_throw() {
    int a[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    Sequence<int>* arr_1 = new ArraySequence<int>(a, 10);
    Sequence<int>* list_1 = new ListSequence<int>(a, 10);
    try
    {
        cout<<arr_1->Get(12);
    }
    catch (...)
    {
        cout << "Error!" << std::endl;
    }

    try
    {
        cout<<list_1->Get(12);
    }
    catch (...)
    {
        cout << "Error!" << std::endl;
    }

    delete arr_1;
}