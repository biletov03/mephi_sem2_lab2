#include "dynamic_array.hpp"

using namespace std;

template<typename Type>

class Matrix {
private:
    DynamicArray<DynamicArray<Type>> * matrix;
    int size_n;
    int size_m;
public:
    //constructs
    Matrix();
    Matrix(int count);
    Matrix(DynamicArray<DynamicArray<Type>> * matrix);
    //geters
    Type Get(int n, int m);
    int GetSize_n();
    int GetSize_m();
    //destructor
    ~Matrix();
};
