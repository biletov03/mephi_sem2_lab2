#include <iostream>
#include "array_sequence.hpp"
#include "list_sequence.hpp"
#include "Sequence.hpp"

using namespace std;

enum Do{
    Get = 1,
    Getlast,
    Getfirst,
    GetAll,
    Getlength,
    Append,
    Prepend,
    Insertat,
    Set,
    Ago
};

void mainMenu() {
    printf("1 - Menu\n");
    printf("2 - Test concat\n");
    printf("3 - Test throw\n");
    printf("4 - Exit\n");

}

void picktipemenu() {
    printf("1 - Int tipe\n");
    printf("2 - Float tip\n");
    printf("3 - Char tipe\n");
    printf("4 - Back\n");
}

void Domenu() {
    printf("1 - Get_in_consol\n");
    printf("2 - Getlast_in_consol\n");
    printf("3 - Getfirst_in_consol\n");
    printf("4 - Getall_in_consol\n");
    printf("5 - Getlength_in_consol\n");
    printf("6 - Append\n");
    printf("7 - Prepend\n");
    printf("8 - Insertat\n");
    printf("9 - Set\n");
    printf("10 - Back\n");

}

template<typename T>
void printAll(Sequence<T> * element) {
    int length = element->GetLength();
    for (int i = 0; i < length; i++)
        cout<<element->Get(i)<<"  ";
    cout<<endl;
}

void Intmenu() {
    Do ch = Ago;
    int choise = ch;
    int index;
        int element;

        Sequence<int> * array = new ArraySequence<int>();
        Sequence<int> * list = new ListSequence<int>();

        do {
            Domenu();
            cin >> choise;
            switch (choise) {
                case (Get):
                    cout << "Input index of geting" << endl;
                    cin >> index;
                    try{cout << "Array  " << array->Get(index) << endl;}
                    catch (...) {cout << "Error!" << std::endl;}

                    try{cout << "List  " << list->Get(index) << endl;}
                    catch (...) {cout << "Error!" << std::endl;}

                    break;
                case (Getlast):
                    cout << "Array  " << array->GetLast() << endl;
                    cout << "List  " << list->GetLast() << endl;
                    break;
                case (Getfirst):
                    cout << "Array  " << array->GetFirst() << endl;
                    cout << "List  " << list->GetFirst() << endl;
                    break;
                case (GetAll):
                    cout << "Array  ";
                    printAll(array);
                    cout << "List  ";
                    printAll(list);
                    break;
                case (Getlength):
                    cout << "Array Length " << array->GetLength() << endl;
                    cout << "List Length " << list->GetLength() << endl;
                    break;
                case (Append):
                    cout << "Input element" << endl;
                    cin >> element;
                    array->Append(element);
                    list->Append(element);
                    break;
                case (Prepend):
                    cout << "Input element" << endl;
                    cin >> element;
                    array->Prepend(element);
                    list->Prepend(element);
                    break;
                case (Insertat):
                    cout << "Input index" << endl;
                    cin >> index;
                    cout << "Input element" << endl;
                    cin >> element;
                    try{array->InsertAt(element, index);}
                    catch (...) {cout << "Error!" << std::endl;}
                    try{list->InsertAt(element, index);}
                    catch (...) {cout << "Error!" << std::endl;}
                    break;
                case (Set):
                    cout << "Input index" << endl;
                    cin >> index;
                    cout << "Input element" << endl;
                    cin >> element;
                    try{array->Set(element, index);}
                    catch (...) {cout << "Error!" << std::endl;}
                    try{list->Set(element, index);}
                    catch (...) {cout << "Error!" << std::endl;}
                    break;
                case (Ago):
                    delete array;
                    delete list;
                    break;
                default:
                    printf("Repid input");
                    break;
            }
        } while(choise != 10);
}

void Floatmenu() {
    Do ch = Ago;
    int choise = ch;
    int index;
    float element;

    Sequence<float> * array = new ArraySequence<float>();
    Sequence<float> * list = new ListSequence<float>();

    do {
        Domenu();
        cin >> choise;
        switch (choise) {
            case (Get):
                cout << "Input index of geting" << endl;
                cin >> index;
                try{cout << "Array  " << array->Get(index) << endl;}
                catch (...) {cout << "Error!" << std::endl;}
                try{cout << "List  " << list->Get(index) << endl;}
                catch (...) {cout << "Error!" << std::endl;}
                break;
            case (Getlast):
                cout << "Array  " << array->GetLast() << endl;
                cout << "List  " << list->GetLast() << endl;
                break;
            case (Getfirst):
                cout << "Array  " << array->GetFirst() << endl;
                cout << "List  " << list->GetFirst() << endl;
                break;
            case (GetAll):
                cout << "Array  ";
                printAll(array);
                cout << "List  ";
                printAll(list);
                break;
            case (Getlength):
                cout << "Array Length " << array->GetLength() << endl;
                cout << "List Length " << list->GetLength() << endl;
                break;
            case (Append):
                cout << "Input element" << endl;
                cin >> element;
                array->Append(element);
                list->Append(element);
                break;
            case (Prepend):
                cout << "Input element" << endl;
                cin >> element;
                array->Prepend(element);
                list->Prepend(element);
                break;
            case (Insertat):
                cout << "Input index" << endl;
                cin >> index;
                cout << "Input element" << endl;
                cin >> element;
                try{array->InsertAt(element, index);}
                catch (...) {cout << "Error!" << std::endl;}
                try{list->InsertAt(element, index);}
                catch (...) {cout << "Error!" << std::endl;}
                break;
            case (Set):
                cout << "Input index" << endl;
                cin >> index;
                cout << "Input element" << endl;
                cin >> element;
                try{array->Set(element, index);}
                catch (...) {cout << "Error!" << std::endl;}
                try{list->Set(element, index);}
                catch (...) {cout << "Error!" << std::endl;}
                break;
            case (Ago):
                delete array;
                delete list;
                break;
            default:
                printf("Repid input");
                break;
        }
    } while(choise != 10);
}

void Charmenu() {
    Do ch = Ago;
    int choise = ch;
    int index;
    char element;

    Sequence<char> * array = new ArraySequence<char>();
    Sequence<char> * list = new ListSequence<char>();

    do {
        Domenu();
        cin >> choise;
        switch (choise) {
            case (Get):
                cout << "Input index of geting" << endl;
                cin >> index;
                try{cout << "Array  " << array->Get(index) << endl;}
                catch (...) {cout << "Error!" << std::endl;}
                try{cout << "List  " << list->Get(index) << endl;}
                catch (...) {cout << "Error!" << std::endl;}
                break;
            case (Getlast):
                cout << "Array  " << array->GetLast() << endl;
                cout << "List  " << list->GetLast() << endl;
                break;
            case (Getfirst):
                cout << "Array  " << array->GetFirst() << endl;
                cout << "List  " << list->GetFirst() << endl;
                break;
            case (GetAll):
                cout << "Array  ";
                printAll(array);
                cout << "List  ";
                printAll(list);
                break;
            case (Getlength):
                cout << "Array Length " << array->GetLength() << endl;
                cout << "List Length " << list->GetLength() << endl;
                break;
            case (Append):
                cout << "Input element" << endl;
                cin >> element;
                array->Append(element);
                list->Append(element);
                break;
            case (Prepend):
                cout << "Input element" << endl;
                cin >> element;
                array->Prepend(element);
                list->Prepend(element);
                break;
            case (Insertat):
                cout << "Input index" << endl;
                cin >> index;
                cout << "Input element" << endl;
                cin >> element;
                try{array->InsertAt(element, index);}
                catch (...) {cout << "Error!" << std::endl;}
                try{list->InsertAt(element, index);}
                catch (...) {cout << "Error!" << std::endl;}
                break;
            case (Set):
                cout << "Input index" << endl;
                cin >> index;
                cout << "Input element" << endl;
                cin >> element;
                try{array->Set(element, index);}
                catch (...) {cout << "Error!" << std::endl;}
                try{list->Set(element, index);}
                catch (...) {cout << "Error!" << std::endl;}
                break;
            case (Ago):
                delete array;
                delete list;
                break;
            default:
                printf("Repid input");
                break;
        }
    } while(choise != 10);
}
